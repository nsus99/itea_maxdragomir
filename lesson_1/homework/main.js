/*

  Задание 1.

  Написать функцию которая будет задавать СЛУЧАЙНЫЙ цвет для фона.
  Каждая перезагрузка страницы будет с новым цветом.
  Для написания используйте функцию на получение случайного целого числа,
  между минимальным и максимальным значением (Приложена снизу задания)

  + Бонус, повесить обработчик на кнопку через метод onClick
  + Бонус, использовать 16-ричную систему исчесления и цвет HEX -> #FFCC00
  + Бонус выводить полученый цвет по центру страницы.
  
  Необходимо создать блок через createElement задать ему стили через element.style
  и вывести через appendChild или insertBefore

  Необходимые материалы:
    Math.Random (Доки): https://developer.mozilla.org/uk/docs/Web/JavaScript/Reference/Global_Objects/Math/random
    function getRandomIntInclusive(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    __
    Работа с цветом:
    Вариант 1.
      Исользовать element.style.background = 'rgb(r,g,b)';
      где r,g,b случайное число от 0 до 255;

    Вариант 2.
      Исользовать element.style.background = '#RRGGBB';
      где, RR, GG, BB, значние цвета в 16-ричной системе исчесления
      Формирование цвета в вебе: https://ru.wikipedia.org/wiki/%D0%A6%D0%B2%D0%B5%D1%82%D0%B0_HTML
      Перевод в 16-ричную систему исчесления делается при помощи
      метода Number.toString( 16 ) https://www.w3schools.com/jsref/jsref_tostring_number.asp,

      var myNumber = '251'
      myNumber.toString(16) // fb

*/

var hex, r, g, b, component,
    app = document.getElementById('app'),
    action = app.querySelector('.action'),
    btn = app.querySelector('.btn'),
    textElem = document.createElement('span');
    textElem.className = 'hex';

    btn.onclick = function() {
        bgColor();
        btn.classList.add('animate');
        btn.setAttribute('disabled', 'true');
         setTimeout(function () {
             btn.classList.remove('animate');
             btn.removeAttribute('disabled');
         }, 1000)
    };


function getRandomIntInclusive(min, max) {

    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;

}

function componentToHex(rgb) {

    rgb = getRandomIntInclusive(0, 255);
    component = rgb.toString(16);
    return component.length === 1 ? "0" + component : component;

}

function bgColor() {

    hex = "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
    app.style.backgroundColor = hex;

    textElem.innerText = hex;
    action.appendChild(textElem);

}

bgColor();