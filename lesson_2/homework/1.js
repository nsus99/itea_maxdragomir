
  /*

    Задание 1.

    Написать скрипт который будет будет переключать вкладки по нажатию
    на кнопки в хедере.

    Главное условие - изменять файл HTML нельзя.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
        + бонус выбрать одним селектором

      2. Повесить кнопку онклик
          button1.onclick = function(event) {

          }
          + бонус: один обработчик на все три кнопки

      3. Написать функцию которая выбирает соответствующую вкладку
        и добавляет к ней класс active

      4. Написать функцию hideAllTabs которая прячет все вкладки.
        Удаляя класс active со всех вкладок

    Методы для работы:

      getElementById
      querySelector
      classList
      classList.add
      forEach
      onclick

      element.onclick = function(event) {
        // do stuff ...
      }

  */


  var buttons = document.getElementById('buttonContainer'),
      tabContainer = document.getElementById('tabContainer'),
      tabItem = tabContainer.querySelectorAll('.tab');

  document.onclick = function(event) {
      hideTabs(event);
  };

  buttons.onclick = function(event) {
      var target = event.target,
          tabNumber = target.dataset.tab;

      if (target.className !== 'showButton') return;

      showTab(tabNumber);

  };

  function showTab(tabNumber) {
      tabItem.forEach( function( item ){
          if(tabNumber === item.dataset.tab) {
              item.classList.toggle('active');
          } else {
              item.classList.remove('active');
          }
      });
  }

  function hideTabs(event) {
      var target = event.target;

      tabItem.forEach( function( item ){
          if (!item.contains(target) && target.className !== 'showButton') {
              item.classList.remove('active');
          }
      });
  }